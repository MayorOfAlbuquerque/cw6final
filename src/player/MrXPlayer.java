package player;

import scotlandyard.*;
import java.util.*;
import java.util.List;

public class MrXPlayer extends AbstractPlayer {

    private int defcon;

    public MrXPlayer(AIGameView aiGameView, DijkstraAI dij, List<Move> moves, int location, Colour player){
        super(aiGameView, dij, moves, location, player);
        this.defcon = setDefcon(location);
    }

    private List<Colour> getDetectives() {              //Gets a list of all detectives in the game
        List<Colour> detectives = new ArrayList<>();
        detectives.addAll(aiGameView.getPlayers());
        detectives.remove(Colour.Black);
        return detectives;
    }

    private List<Integer> findMoveWithNode(List<Move> moves, int node) {
        List<Integer> nodeList = new ArrayList<Integer>();                                       //Finds moves from a list which go to a specific node
        for(Move move : moves) {
            if(move instanceof MoveTicket) {
                if( ((MoveTicket) move).target == node) {
                    nodeList.add(moves.indexOf(move));
                }
            }
            if(move instanceof MoveDouble) {
                if( ((MoveDouble) move).move1.target == node || ((MoveDouble) move).move2.target == node) {
                    nodeList.add(moves.indexOf(move));
                }
            }
        }
        return nodeList;
    }

    private int smallestDistanceToDetectives(int location) {
        int smallestDist = 100;
        for(Colour det : getDetectives()) {
            int dist = aiGameView.locatePlayer(det) - location;
            if(dist < 0) {
                dist = dist*-1;
            }
            if(dist < smallestDist) {
                smallestDist = dist;
            }
        }
        return smallestDist;
    }

    private void setMoveScore(int[] moveScores, List<Integer> listOfIndexes, int valToAdd) {         //Adds value to moves scores
        for (Integer node : listOfIndexes) {
            moveScores[node] = moveScores[node] + valToAdd;
        }
    }

    private int prioritiseTicketType(Ticket ticket, boolean isDouble, boolean defconPriority) {
        if(isDouble && ticket.equals(Ticket.Secret) && defconPriority) {
            return 7;
        }
        else if(!isDouble && ticket.equals(Ticket.Secret) && defconPriority) {
            return 10;
        }
        return 0;
    }

    private int prioritiseHigherDistances(boolean defconPriority, int target) {
        if((target > location + 10 && defconPriority) || (target < location - 10 && defconPriority)) {
            return 5;
        }
        if((target > location + 20 && defconPriority) || (target < location - 20 && defconPriority)){
            return 10;
        }
        return 0;
    }

    private int prioritiseMoveType(boolean isDouble, boolean defconPriority) {
        if(isDouble && defconPriority) {
            return 7;
        }
        else if(!isDouble && !defconPriority) {
            return 7;
        }
        return 0;
    }


    public int scoreMove (Move move) {

        boolean defconPriority;
        if(defcon < 2) {
            defconPriority = true;
        }
        else {
            defconPriority = false;
        }
        int score = 0;
        int moveLocation = getTarget(move, location);
        if(move instanceof MoveDouble) {
            int temp = prioritiseMoveType(true, defconPriority);
            if(((MoveDouble) move).move1.ticket == Ticket.Secret)
                temp = temp + prioritiseTicketType(Ticket.Secret, true, defconPriority);
            if(((MoveDouble) move).move2.ticket == Ticket.Secret) {
                temp = temp + prioritiseTicketType(Ticket.Secret, true, defconPriority);
            }
            temp = temp + prioritiseHigherDistances(defconPriority, moveLocation);
            if(smallestDistanceToDetectives(moveLocation) < 5) {
                temp = temp - 5;
            }
            score = temp;
        }
        if( move instanceof MoveTicket) {
            int temp = prioritiseMoveType(false, defconPriority);
            temp = temp + prioritiseTicketType(((MoveTicket) move).ticket, false, defconPriority);
            temp = temp + prioritiseHigherDistances(defconPriority, moveLocation);
            if(smallestDistanceToDetectives(moveLocation) < 5) {
                temp = temp - 5;
            }
            score = temp;
        }
        return score;
    }

    //generate a defcon number depending on whether any detective is within 4 spaces of mr x
    //and whether he is currently revealed
    private int setDefcon(int location) {
        //set to 3 by default
        int defconStatus = 3;

        //if he is revealed set to 1
        if(aiGameView.getRounds().get(aiGameView.getRound())) {
            defconStatus = defconStatus - 1;
            return defconStatus;
        }

        //else set to 2 if any detective is within 4 places of him
        for(Colour det : getDetectives()){
            if(distance(aiGameView.locatePlayer(det), location, det) <= 4){
                defconStatus = 2;
                return defconStatus;
            }
        }
        return defconStatus;
    }

    //create a map of Moves with their respective scores
    public Map<Move, Integer> scoreMoves() {

        //get list of detectives
        List<Colour> detectives = getDetectives();

        //make a new array for holding the scores for each move
        int[] moveScores = new int[moves.size()];
        for(int i = 0; i < moveScores.length; i++) {
            moveScores[i] = 0;
        }

        //for each detective,
        for(Colour det : detectives) {

            List<Integer> quickestRoute = bestRoute(aiGameView.locatePlayer(det), location, det);
            if (quickestRoute.size() > 2) {

                int nodeToPlayer = quickestRoute.get(quickestRoute.size() - 2);

                if (quickestRoute.size() <= 4) {
                    List<Integer> listOfIndexes = findMoveWithNode(moves, nodeToPlayer);
                    setMoveScore(moveScores, listOfIndexes, -7);
                }
                else if (quickestRoute.size() <= 6) {
                    List<Integer> listOfIndexes = findMoveWithNode(moves, nodeToPlayer);
                    setMoveScore(moveScores, listOfIndexes, -5);
                }
                else {
                    List<Integer> listOfIndexes = findMoveWithNode(moves, nodeToPlayer);
                    setMoveScore(moveScores, listOfIndexes, -3);
                }
            }
            else {
                int nodeToPlayer = quickestRoute.get(0);
                List<Integer> listOfIndexes = findMoveWithNode(moves, nodeToPlayer);
                setMoveScore(moveScores, listOfIndexes, -25);
            }
        }

        Map<Move, Integer> moveMap = new HashMap<Move, Integer>();
        for (Move move : moves){
            moveMap.put(move, scoreMove(move));
        }

        return moveMap;
    }
}
