package player;

import scotlandyard.*;
import scotlandyard.Ticket;
import java.util.*;
import java.util.List;
import graph.*;

public class DetectivePlayer extends AbstractPlayer {
    private Map<Transport, Integer> tickets;
    private List<Move> validMoves; //validmoves contains all moves which will not leave the detective stranded
    private Graph<Integer, Transport> graph;
    private TicketList ticketList;
    private int destination;

    public DetectivePlayer(AIGameView aiGameView, DijkstraAI dij, List<Move> moves, int location, Colour player, TicketList ticketList) {
        super(aiGameView, dij, moves, location, player);
        this.tickets = aiGameView.getTransport(player);
        this.graph = dij.getGraph();
        this.validMoves = new ArrayList<>();
        this.ticketList = ticketList;
        removeStrandMoves();
        decideDestination();
    }

    public void decideDestination(){
        int mrXLocation;
        if (aiGameView.getRound() < 3){ //rounds before Mr. X is revealed.
            mrXLocation = 114; //approximately the middle of the graph.
        }
        else{
            mrXLocation = aiGameView.locatePlayer(Colour.Black);
        }

        List<Node> locationList = new ArrayList<>();
        locationList.add(graph.getNode(mrXLocation));

        //this is returning an empty list
        List<Ticket> MrXTicketList = ticketList.getMrXTickets();

        List<List<Transport>> transports = ticketToTransport(MrXTicketList);

        List<Node> possibleDestinations = generateDestinations(locationList, transports);
        Collections.shuffle(possibleDestinations);

        /*
        System.out.println("-------possibleDestinations.size() = " + possibleDestinations.size());
        for (int i = 0; i < possibleDestinations.size(); i++){
            if (possibleDestinations.get(i) == null){
                System.out.println("-------possibleDestinations.get(" + i + ") = null");
            }
            else{
                System.out.println("-------possibleDestinations.get(" + i + ") = " + possibleDestinations.get(i));
            }
        }
        */
        destination = (Integer) possibleDestinations.get(0).getIndex();
    }

    public List<Node> generateDestinations(List<Node> nodes, List<List<Transport>> transports){
        if (transports.isEmpty()){
            return nodes;
        }

        List<Node> destinations = new ArrayList<>();

        for (Node node : nodes){
            List<Node> newNodes = step(node, transports.get(0));
            transports.remove(transports.get(0));
            destinations.addAll(generateDestinations(newNodes, transports));
        }
        return destinations;
    }

    //method takes in a node and returns a list of nodes which are connected to that node via any transport
    //type from a set list of valid transport types depending on the ticket which Mr. X used
    public List<Node> step (Node<Integer> node, List<Transport> transports){
        List<Node> nodes = new ArrayList<>();

        for (Edge<Integer, Transport> edge : graph.getEdgesFrom(node)) {
            if (transports.contains(edge.getData())){
                nodes.add(edge.getTarget());
            }
        }
        return nodes;
    }

    //method takes in a list of tickets used by Mr. X and creates a new list containing lists of transport
    //types. Each list of transport types corresponds to a single ticket and expresses which transport types
    //are could have been used with this ticket.
    public List<List<Transport>> ticketToTransport (List<Ticket> tickets){
        List<List<Transport>> transports = new ArrayList<>();
        for (Ticket ticket : tickets){
            List<Transport> transport = new ArrayList<>();
            switch (ticket) {
                case Taxi:
                    transport.add(Transport.Taxi);
                case Bus:
                    transport.add(Transport.Bus);
                case Underground:
                    transport.add(Transport.Underground);
                case Secret:
                    transport.add(Transport.Taxi);
                    transport.add(Transport.Bus);
                    transport.add(Transport.Underground);
                    transport.add(Transport.Boat);
            }
            transports.add(transport);
        }
        return transports;
    }

    public Map<Move, Integer> scoreMoves() {
        Map<Move, Integer> moveScores = new HashMap<>();
        for (Move move : validMoves) {
            moveScores.put(move, scoreMove(move));
        }
        return moveScores;
    }

    public int scoreMove(Move move) {
        return -distance(getTarget(move, aiGameView.locatePlayer(player)), destination, player);
    }

    public void removeStrandMoves() {
        if (!(moves.get(0) instanceof MovePass)) {
            for (Move move : moves) {
                int target = getTarget(move, aiGameView.locatePlayer(player));
                Node<Integer> node = graph.getNode(target);
                List<Edge<Integer, Transport>> edges = graph.getEdgesFrom(node);
                if (((tickets.get(Transport.Taxi) > 0) && (checkEdges(edges, Transport.Taxi)))
                        || ((tickets.get(Transport.Bus) > 0) && (checkEdges(edges, Transport.Bus)))
                        || ((tickets.get(Transport.Underground) > 0) && (checkEdges(edges, Transport.Underground)))) {
                    validMoves.add(move);
                }
            }
            if (validMoves.isEmpty()) {
                validMoves.addAll(moves);
            }
        }
    }

    //checks if any of the edges from a list of edges uses a particular transport type
    public boolean checkEdges(List<Edge<Integer, Transport>> edges, Transport transport) {
        for (Edge<Integer, Transport> edge : edges) {
            if (edge.getData() == transport) {
                return true;
            }
        }
        return false;
    }

    /*
    //returns whether a player can travel on their specified transport type
    public boolean canTravel(Map<Transport, Integer> tickets, Transport transport) {
        if (tickets.get(transport) > 0) {
            return true;
        }
        return false;
    }
    */

    /*
    public int nearestStation(List<Node<Integer>> nodes, List<Node<Integer>> visitedNodes, Transport transport, Map<Transport, Integer> tickets){
        //for each node in the list, check all its edges
        //if any are of the correct transport type,
        //return that node as being the nearestStation
        for (Node node : nodes){
            List<Edge<Integer, Transport>> edges = graph.getEdgesFrom(node);
            for (Edge<Integer, Transport> edge : edges) {
                Transport edgeTransport = edge.getData();
                if (edgeTransport == transport) {
                    return (Integer) node.getIndex();
                }
            }
        }

        //otherwise add all current nodes to the list of visited nodes and create a new list of nodes
        //based on whether the player has enough tickets to reach the node and whether the nodes has already
        //been visited
        visitedNodes.addAll(nodes);
        List<Node<Integer>> newNodes = new ArrayList<Node<Integer>>();
        for (Node node : nodes){
            List<Edge<Integer, Transport>> edges = graph.getEdgesFrom(node);
            for (Edge<Integer, Transport> edge : edges) {
                Transport edgeTransport = edge.getData();
                if (tickets.get(edgeTransport) > 0) && (edge.getTarget()){

                }
            }
        }
        if (newNodes.isEmpty()){
            return 0;
        }
        else{
            return nearestStation(newNodes, visitedNodes, transport, newTickets);
        }
    */


}