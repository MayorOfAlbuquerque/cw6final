package player;

import scotlandyard.*;
import java.util.*;
import java.util.List;
import java.util.Map;


public class TreeAI implements Player {

    protected ScotlandYardView view;
    protected DijkstraAI d;
    protected AIGameView aiGameView;
    protected TicketList ticketList;
    ScotlandYardGraph graph;

    public TreeAI(ScotlandYardView view, String graphFileName, TicketList ticketList) {
        this.ticketList = ticketList;
        this.view = view;
        this.d = new DijkstraAI(graphFileName);
        try {
            ScotlandYardGraphReader reader = new ScotlandYardGraphReader();
            graph = reader.readGraph(graphFileName);
        } catch (Exception e) {
            System.err.println(e);
            e.printStackTrace();
            System.exit(1);
        }
    }



    private Map<Move, Integer> treeScores(int treeDepth, int location, List<Move> moves, AIGameView newView) {
        Map<Move, Integer> scoreMap = new HashMap<Move, Integer>();
        int x = 0;
        int highestMoveScore = -10000;
        treeDepth = treeDepth-1;
        for (Move move : moves) {
            x = x+1;
            if (newView.getCurrentPlayer() == Colour.Black) {
                MrXPlayer xMoves = new MrXPlayer(newView, d, moves, location, Colour.Black);
                int moveScore = xMoves.scoreMove(move);
                AIStore store = updateAiView(move, newView, location);
                Colour currentP = newView.getCurrentPlayer();
                List<Move> theseMoves = graph.generateMoves(currentP, newView.locatePlayer(currentP));
                if (treeDepth > 0) {
                    highestMoveScore = moveScore + getBestMoveValue(treeScores(treeDepth, newView.locatePlayer(currentP), theseMoves, newView));
                } else {
                    highestMoveScore = moveScore;
                }
                revertView(store, newView, true);
            }
            else {
                DetectivePlayer detectivePlayer = new DetectivePlayer(newView, d, moves, location, newView.getCurrentPlayer(), ticketList);
                int moveScore = detectivePlayer.scoreMove(move);
                AIStore store =updateAiView(move, newView, location);
                Colour currentP = newView.getCurrentPlayer();

                List<Move> theseMoves = graph.generateMoves(currentP, newView.locatePlayer(currentP));
                if(treeDepth > 0) {
                    highestMoveScore = moveScore + getBestMoveValue(treeScores(treeDepth, newView.locatePlayer(currentP), theseMoves, newView));
                }
                else {
                    highestMoveScore = moveScore;
                }
                revertView(store, newView, false);

            }

            scoreMap.put(move, highestMoveScore);
        }

        return scoreMap;
    }

    private void revertView(AIStore store, AIGameView aiGameView, boolean black) {
        aiGameView.movePlayer(store.player, store.location);
        if(black) {
            aiGameView.redRound();
        }

        aiGameView.currentPlayer = store.player;
    }


    private AIStore updateAiView(Move move, AIGameView newView, int location) {
        AIStore store = new AIStore(location, newView.getCurrentPlayer(), newView.getRound());
        int target;
        if(move instanceof MoveDouble) {
            target = ((MoveDouble) move).move2.target;
        }
        else if (move instanceof MoveTicket) {
            target = ((MoveTicket) move).target;
        }
        else {
            target = newView.locatePlayer(newView.getCurrentPlayer());
        }
        if(newView.getCurrentPlayer() == Colour.Black) {
            newView.incrRound();
        }
        newView.movePlayer(newView.getCurrentPlayer(), target);
        newView.incrplayers();
        return store;
    }


    private int getBestMoveValue(Map<Move, Integer> scoreMap) {
        int x = -100;
        for (Map.Entry<Move, Integer> entry : scoreMap.entrySet()) {
            if (entry.getValue() > x) {
                x = entry.getValue();
            }
        }
        return x;
    }

    //take in the location of the current player and the list of their valid moves and play their preferred move
    @Override
    public void notify(int location, List<Move> moves, Integer token, Receiver receiver) {
        AIGameView newView = new AIGameView(view);
        Map<Move, Integer> scoreMap = treeScores(6, location, moves, newView);

        //play the actual move
        Map.Entry<Move, Integer> maxEntry = null;
        for (Map.Entry<Move, Integer> entry : scoreMap.entrySet()) {
            if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0) {
                maxEntry = entry;
            }
        }
        Move moveToPlay = maxEntry.getKey();
        updateAiView(moveToPlay, newView, location);
        receiver.playMove(moveToPlay, token);
    }
}