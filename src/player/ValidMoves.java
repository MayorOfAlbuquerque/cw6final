package player;

import graph.*;
import scotlandyard.*;

import java.util.*;

public class ValidMoves extends UndirectedGraph<Integer, Transport> {

    private int location;
    private Colour colour;
    private Map<Ticket, Integer> tickets;
    private AIGameView aiGameView;

    public ValidMoves(Colour colour, int location, Map<Ticket, Integer> tickets, AIGameView aiGameView) {
        this.colour = colour;
        this.location = location;
        this.tickets = tickets;
        this.aiGameView = aiGameView;
    }

    public List<Move> generateMoves() {

        List<Move> validmoves = new ArrayList<Move>();                                                          //create a list of valid moves

        List<MoveTicket> validMoveTickets = generateSingleMoves(colour, location, tickets);         //generate a list of valid single moves
        validmoves.addAll(validMoveTickets);                                                        //add the list of single moves to the list of valid moves

        if ((colour == Colour.Black) && (tickets.get(Ticket.Double) > 0)){                                  //if player is mr. x and has double tickets then create
            for (MoveTicket move1 : validMoveTickets){                                                      //valid double moves for each valid single move
                Map<Ticket, Integer> newtickets = new HashMap<Ticket, Integer>();
                newtickets.putAll(tickets);
                newtickets.put(move1.ticket, (tickets.get(move1.ticket)-1));                           //remove the ticket which was used in the initial move
                List<MoveTicket> recurseMoveTickets = generateSingleMoves(colour, move1.target, newtickets); //types conflict
                for (MoveTicket move2 : recurseMoveTickets){
                    MoveDouble movedouble = MoveDouble.instance(colour, move1, move2);
                    validmoves.add(movedouble);
                }
            }
        }

        validmoves = removeOccupiedNodes(validmoves);

        return validmoves;
    }

    public List<MoveTicket> generateSingleMoves(Colour colour, int location, Map<Ticket, Integer> tickets) {

        List<Edge<Integer,Transport>> connectedEdges = getEdgesFrom(getNode(location));
        System.out.println("loc = " + location);
        System.out.println("getNode = " + getNode(location));
        Boolean taxi, bus, underground;
        taxi = (tickets.get(Ticket.Taxi) > 0);
        bus = (tickets.get(Ticket.Bus) > 0);
        underground = (tickets.get(Ticket.Underground) > 0);

        List<MoveTicket> validMoveTickets = new ArrayList<MoveTicket>();
        for (Edge<Integer,Transport> edge : connectedEdges){
            if ((edge.getData() == Transport.Taxi) && (taxi) ){
                MoveTicket move = MoveTicket.instance(colour, Ticket.Taxi, edge.getTarget().getIndex());
                validMoveTickets.add(move);
            }
            else if ((edge.getData() == Transport.Bus) && (bus) ){
                MoveTicket move = MoveTicket.instance(colour, Ticket.Bus, edge.getTarget().getIndex());
                validMoveTickets.add(move);
            }
            else if ((edge.getData() == Transport.Underground) && (underground) ){
                MoveTicket move = MoveTicket.instance(colour, Ticket.Underground, edge.getTarget().getIndex());
                validMoveTickets.add(move);
            }
        }
        if ((colour == Colour.Black) && (tickets.get(Ticket.Secret) > 0)){
            for (Edge<Integer,Transport> edge : connectedEdges){
                MoveTicket move = MoveTicket.instance(colour, Ticket.Secret, edge.getTarget().getIndex());
                validMoveTickets.add(move);

            }
        }
        return validMoveTickets;
    }


    private List<Move> removeOccupiedNodes(List<Move> moves) {

        List<Move> newMoves = new ArrayList<Move>();
        newMoves.addAll(moves);
        for(Move move : moves){
            if(move instanceof MoveTicket) {
                for(Colour player : aiGameView.players) {
                    if ((player != Colour.Black) && (aiGameView.locatePlayer(player) ==(((MoveTicket) move).target))) {
                        newMoves.remove(move);
                    }
                }
            }
            if(move instanceof MoveDouble) {
                List<Move> moveCheck = new ArrayList<Move>();
                moveCheck.add(((MoveDouble) move).move1);
                moveCheck.add(((MoveDouble) move).move2);
                moveCheck = removeOccupiedNodes(moveCheck);
                if (moveCheck.size() != 2){
                    newMoves.remove(move);
                }
            }
        }
        return newMoves;
    }
}