package player;

import scotlandyard.*;
import java.util.List;

public class BasicPlayer implements Player, Spectator {

    protected ScotlandYardView view;
    protected DijkstraAI d;
    protected AIGameView aiGameView;
    protected TicketList ticketList;

    public BasicPlayer(ScotlandYardView view, String graphFilename, TicketList ticketList) {
        this.view = view;
        this.d = new DijkstraAI(graphFilename);
        this.ticketList = ticketList;
    }

    //This spectator method is used to store the tickets used by Mr. X during his moves in the TicketList
    //Currently this method is not ever being called.
    public void notify(Move move){
        System.out.println("-------------Notify called");
        if (move.colour == Colour.Black){
            if (view.getRounds().get(view.getRound())){
                System.out.println("----------Reseting ticket list");
                ticketList.resetMrXMoves();
            }
            else{
                System.out.println("----------Adding ticket to list");
                Ticket ticket1;
                if (move instanceof MoveTicket){
                    ticket1 = ((MoveTicket) move).ticket;
                    ticketList.addToList(ticket1);
                }
                else if (move instanceof MoveDouble){
                    ticket1 = ((MoveDouble) move).move1.ticket;
                    Ticket ticket2 = ((MoveDouble) move).move2.ticket;
                    ticketList.addToList(ticket1);
                    ticketList.addToList(ticket2);
                }
            }
        }
    }

    //take in the location of the current player and the list of their valid moves and play their preferred move
    @Override
    public void notify(int location, List<Move> moves, Integer token, Receiver receiver) {
        System.out.println("-------------Notify Turn called");
        //creating an AI to choose a move
        this.aiGameView = new AIGameView(view);
        Move moveToPlay;

        if(view.getCurrentPlayer() == Colour.Black) {
            System.out.println("Player is Mr. X");
            MrXPlayer mrXPlayer = new MrXPlayer(aiGameView, d, moves, location, view.getCurrentPlayer());
            moveToPlay = mrXPlayer.getMove();
            if (view.getRounds().get(view.getRound())){
                System.out.println("----------Resetting ticket list");
                ticketList.resetMrXMoves();
            }
            else{
                System.out.println("----------Adding ticket to list");
                Ticket ticket1;
                if (moveToPlay instanceof MoveTicket){
                    ticket1 = ((MoveTicket) moveToPlay).ticket;
                    ticketList.addToList(ticket1);
                }
                else if (moveToPlay instanceof MoveDouble){
                    ticket1 = ((MoveDouble) moveToPlay).move1.ticket;
                    Ticket ticket2 = ((MoveDouble) moveToPlay).move2.ticket;
                    ticketList.addToList(ticket1);
                    ticketList.addToList(ticket2);
                }
            }
        }
        else {
            System.out.println("Player is a Detective");
            DetectivePlayer detectivePlayer = new DetectivePlayer(aiGameView, d, moves, location, view.getCurrentPlayer(), ticketList);
            moveToPlay = detectivePlayer.getMove();
        }

        receiver.playMove(moveToPlay, token);
    }

}