package player;

import scotlandyard.Colour;
import scotlandyard.Ticket;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TicketList {
    private List<Ticket> mrXTickets;
    private Map<Colour, Integer> detectiveDestinations;

    public TicketList(){
        mrXTickets = new ArrayList<>();
        detectiveDestinations = new HashMap<>();
    }

    public void addToList(Ticket ticket){
        System.out.println("-------ticket added to list");
        mrXTickets.add(ticket);
    }
    public List<Ticket> getMrXTickets(){
        return mrXTickets;
    }
    public void resetMrXMoves(){
        System.out.println("-------ticket list reset");
        mrXTickets = new ArrayList<>();
    }
}
