package player;

import scotlandyard.*;
import java.util.List;
import java.util.Map;

public abstract class AbstractPlayer{

    protected AIGameView aiGameView;
    protected DijkstraAI dijGraph;
    protected List<Move>moves;
    protected int location;
    protected Colour player;

    public AbstractPlayer(AIGameView aiGameView, DijkstraAI dij, List<Move> moves, int location, Colour colour){
        this.dijGraph=dij;
        this.aiGameView = aiGameView;
        this.moves = moves;
        this.location = location;
        this.player = colour;
    }

    public List<Integer> bestRoute(int location, int destination, Colour player){
        List<Integer> route = dijGraph.getRoute(location, destination, aiGameView.getTransport(player));
        return route;
    }

    //returns number of nodes to travel to before you reach the destination node
    //and based on which tickets you have available
    public int distance(int location, int destination, Colour player){
        List<Integer> route = bestRoute(location, destination, player);
        return route.size();
    }

    //will return the integer representing the node which will moved to after a move of any type
    public int getTarget(Move move, int startLocation){
        int targetLocation;
        if (move instanceof MoveDouble){
            targetLocation = ((MoveDouble) move).move2.target;
        }
        else if (move instanceof MoveTicket){
            targetLocation = ((MoveTicket) move).target;
        }
        else{
            targetLocation = startLocation;
        }
        return targetLocation;
    }

    //returns a score for a move
    public abstract int scoreMove(Move move);

    //returns a map of moves with their associated scores
    public abstract Map<Move, Integer> scoreMoves();

    //finds the key in a map with the highest value
    public Map.Entry<Move, Integer> maxEntry(Map<Move, Integer> scoreMap){
        Map.Entry<Move, Integer>maxEntry=null;
        for(Map.Entry<Move, Integer>entry:scoreMap.entrySet()){
            if(maxEntry==null||entry.getValue().compareTo(maxEntry.getValue())>0){
                maxEntry=entry;
            }
        }
        return maxEntry;
    }

    //returns the move with the highest score from the map of moves and scores
    public Move getMove(){
        Map<Move, Integer> scoreMap = scoreMoves();
        Move chosenMove = maxEntry(scoreMap).getKey();
        return chosenMove;
    }

}