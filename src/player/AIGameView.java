package player;


import scotlandyard.*;

import java.util.*;
import java.util.List;
import java.lang.Enum;

public class AIGameView{

    protected ScotlandYardView view;
    protected Map<Colour, Integer> playerLocations;
    protected Map<Colour, Map<Ticket, Integer>> playerTickets;
    protected int round;
    protected List<Boolean> rounds;
    protected List<Colour> players;
    protected Colour currentPlayer;
    protected Map<Colour, Map<Transport, Integer>> playerTransport;

    public AIGameView(ScotlandYardView view) {
        this.view = view;
        playerLocations = new HashMap<Colour, Integer>();
        List<Colour> players = view.getPlayers();
        playerTickets = new HashMap<Colour, Map<Ticket, Integer>>();
        playerTransport = new HashMap<Colour, Map<Transport, Integer>>();
        for(Colour player : players) {
            playerLocations.put(player, view.getPlayerLocation(player));
            playerTickets.put(player, setTickets(player));
            playerTransport.put(player, setTransport(player));
        }
        this.rounds = view.getRounds();
        this.players = view.getPlayers();


        this.round = view.getRound();
        this.currentPlayer = view.getCurrentPlayer();
    }

    public void movePlayer(Colour player, int newLocation) {
        playerLocations.remove(player);
        playerLocations.put(player, newLocation);
    }

    public int locatePlayer(Colour player) {
        return playerLocations.get(player);
    }

    public List<Colour> getPlayers(){
        return players;
    }


    public Map<Ticket, Integer> setTickets (Colour player) {
        Map<Ticket, Integer> tickets = new HashMap<Ticket, Integer>();
        tickets.put(Ticket.Bus, view.getPlayerTickets(player, Ticket.Bus));
        tickets.put(Ticket.Taxi, view.getPlayerTickets(player, Ticket.Taxi));
        tickets.put(Ticket.Underground, view.getPlayerTickets(player, Ticket.Underground));
        return tickets;
    }

    public Map<Transport, Integer> setTransport (Colour player) {
        Map<Transport, Integer> tickets = new HashMap<Transport, Integer>();
        tickets.put(Transport.Bus, view.getPlayerTickets(player, Ticket.Bus));
        tickets.put(Transport.Taxi, view.getPlayerTickets(player, Ticket.Taxi));
        tickets.put(Transport.Underground, view.getPlayerTickets(player, Ticket.Underground));
        return tickets;
    }


    public Map<Transport, Integer> getTransport(Colour player) {
        return playerTransport.get(player);
    }

    public Map<Ticket, Integer> getTickets(Colour player) {
        return playerTickets.get(player);
    }

    public int getRound() {
        return round;
    }

    public void incrRound() {
        round = round+1;
    }

    public void redRound() {
        round = round-1;
    }

    public List<Boolean> getRounds() {
        return rounds;
    }

    public Colour getCurrentPlayer() {
        return currentPlayer;
    }

    public void incrplayers() {
        int index = players.indexOf(getCurrentPlayer());
        if(players.size() == index+1) {
            currentPlayer = players.get(0);
        }
        else {
            currentPlayer = players.get(index+1);
        }
    }
}
